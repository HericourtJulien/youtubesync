$(document).ready(function () {
    loadPlaylist();
});

function loadEvents() {
    $('#wrapper .block-btn').on('click', function () {
        convert2mp3($(this).data('id'));
    });
}

function loadPlaylist() {
    var plId = 'PLqKizhk9M9_1y9fWGbV5qktnySD_aEE_f';
    var key = 'AIzaSyC3Kq0lyMoE4Tp2paGMUgCM_ZGIHQdUrsA';
    $.ajax({
        url: 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=30&playlistId=' + plId + '&key=' + key,
        success: displayVideos,
        error: function () {
            console.log('Error');
        }
    })
}

function displayVideos(res) {
    var i = 0, nbVideos = 30;
    var result, el;
    for (i; i < nbVideos - 1; i++) {

        // $.ajax({
        //    url: 'http://www.youtubeinmp3.com/fetch/?format=JSON&video=http://www.youtube.com/watch?v=' + res.items[i].snippet.resourceId.videoId,
        //     success: pushInfoToView,
        //     error: function () {
        //         console.log('Error 2');
        //     }
        // });
        // console.log(res);
        el = res.items[i].snippet;
        console.log(res);

        result = '<div class="block">';
        result += '<div class="block-img">';
        result += '<img src="' + el.thumbnails.high.url + '" width="' + el.thumbnails.high.width + '" height="' + el.thumbnails.high.height + '">';
        result += '</div>';
        result += '<div class="block-title">' + el.title + '</div>';
        result += '<div class="block-btns">';
        result += '<div class="block-btn btn-dl" data-id="' + el.resourceId.videoId + '"><span>Télécharger</span></div>';
        result += '<div class="block-btn btn-eye" data-id="' + el.resourceId.videoId + '"><a href="https://www.youtube.com/watch?v=' + el.resourceId.videoId + '"></a></div>';
        result += '</div>';
        result += '</div>';

        $('#wrapper').append(result);
    }
    loadEvents();
}


var video = '';

function convert2mp3(t) {
    $('.c2m3').html('Converting... Please wait... Download starts automatically');
    video = t;

    // $.get("http://api.convert2mp3.cc/check.php?api=true&v=" + t + "&h=" + Math.floor(35e5 * Math.random()), function (t) {
    //     var o = t.split("|");
    //     return "OK" == o[0] ? void(window.location.href = "http://dl" + o[1] + ".downloader.space/dl.php?id=" + o[2]) : void(("ERROR" != o[0] || 1 != o[1] && 5 != o[1]) && setTimeout("convert2mp3(video)", 5e3))
    // })

    $.ajax({
        url: 'http://api.convert2mp3.cc/check.php?api=true&v=' + t + '&h=' + Math.floor(35e5 * Math.random()),
        success: function (t) {
            var o = t.split('|');
            var dlLink = 'http://dl' + o[1] + '.downloader.space/dl.php?id=' + o[2];
            return 'OK' == o[0] ? cordova.InAppBrowser.open(dlLink, '_self', 'location=no') : void(('ERROR' != o[0] || 1 != o[1] && 5 != o[1]) && setTimeout('convert2mp3(video)', 5e3))
        },
        error: function () {
            console.log('Error');
        }
    })
}

